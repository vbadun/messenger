﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Messager.Controllers
{
    public class HomeController : Controller
    {
        private readonly MessageRepository mess;
        private readonly CategoryRepository cat;

        public HomeController()
        {
            mess = new MessageRepository();
            cat = new CategoryRepository();
        }

        public ActionResult Delete(Guid id)
        {
            var adress = Server.HtmlEncode(Request.UserHostAddress);
            var message = mess.GetByID(id);

            if (!message.PublisherHostAddress.Contains(adress))
            {
                return View("ShowNoAllow");
            }
            else
            {
                mess.Delete(id);
                return RedirectToAction("Index");
            }

        }
        [HttpGet]
        public ActionResult Details(Guid id)
        {
            var entry = mess.GetByID(id);
            return View(entry);
        }
        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            var adress = Server.HtmlEncode(Request.UserHostAddress);
            var message = mess.GetByID(id);

            if (!message.PublisherHostAddress.Contains(adress))
            {
                return View("ShowNoAllow");
            }
            else
            {
                return View(message);
            }

        }
        [HttpPost]
        public ActionResult Edit(Message message, Guid id)
        {
            mess.Edit(message, id);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult SetPositiveRating(Guid id)
        {
            var adress = Server.HtmlEncode(Request.UserHostAddress);
            mess.SetPositiveRating(id, adress);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult SetNegativeRating(Guid id)
        {
            var adress = Server.HtmlEncode(Request.UserHostAddress);
            mess.SetNegativeRating(id, adress);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Create()
        {
            SelectList categories = new SelectList(cat.GetAllItems().ToList(), "CategoryName", "CategoryName");
            ViewBag.Categories = categories;
            return View();
        }

        [HttpPost]
        public ActionResult Create(Message message)
        {
            var adress = Server.HtmlEncode(Request.UserHostAddress);
            var mes = new Message
            {
                Id = Guid.NewGuid(),
                Body = message.Body,
                Author = message.Author,
                Header = message.Header,
                Rating = 0,
                TimeOfCreate = DateTime.Now,
                LastModifyDate = DateTime.Now,
                RaitingChangeHostAddress = new List<string>(),
                PublisherHostAddress = adress,
                BodySize = message.Body.Length,
                CategoryName = message.CategoryName,
            };
            mess.Add(mes);
            return RedirectToAction("Index");
        }
        public ActionResult Index(string sortOrder, string searchHeader, DateTime? fromtime, DateTime? tilltime, string searchCategory, string SearchAuthor)
        {
            ViewBag.HeaderSortParm = String.IsNullOrEmpty(sortOrder) ? "Header desc" : "";
         //   ViewBag.HeaderSortParm = sortOrder == "Header" ? "Header desc" : "Header";
            ViewBag.CategorySortParm = sortOrder == "Category" ? "Category desc" : "Category";
            ViewBag.AuthorSortParm = sortOrder == "Author" ? "Author desc" : "Author";
            ViewBag.DateSortParm = sortOrder == "Date" ? "Date desc" : "Date";
            var list = mess.GetAllItems();
            if (!String.IsNullOrEmpty(searchHeader))
            {
                list = list.Where(x => x.Header.ToUpper().Contains(searchHeader.ToUpper()));
            }
            if (!String.IsNullOrEmpty(searchCategory))
            {
                list = list.Where(x => x.CategoryName.ToUpper().Contains(searchCategory.ToUpper()));
            }
            if (!String.IsNullOrEmpty(SearchAuthor))
            {
                list = list.Where(x => x.Author.ToUpper().Contains(SearchAuthor.ToUpper()));
            }
            if ( fromtime.HasValue)
            {
                list = list.Where(x => x.TimeOfCreate.Date >= fromtime);
            }
            if (tilltime.HasValue)
            {
                list = list.Where(x => x.TimeOfCreate.Date <= tilltime);
            }

            switch (sortOrder)
            {
                case "Header desc":
                    list = list.OrderByDescending(x => x.Header);
                    break;
                case "Category desc":
                    list = list.OrderByDescending(x => x.CategoryName);
                    break;
                case "Category":
                    list = list.OrderBy(x => x.CategoryName);
                    break;
                case "Author desc":
                    list = list.OrderByDescending(x => x.Author);
                    break;
                case "Author":
                    list = list.OrderBy(x => x.Author);
                    break;
                case "Date desc":
                    list = list.OrderByDescending(x => x.TimeOfCreate);
                    break;
                case "Date":
                    list = list.OrderBy(x => x.TimeOfCreate);
                    break;
                default:
                    list =list.OrderByDescending(x => x.TimeOfCreate);
                    break;
            }
         
            return View(list.Take(10));
        }
        public ActionResult Categories()
        {
            var catlist = cat.GetAllItems();
            return View(catlist);
        }

        [HttpGet]
        public ActionResult CreateCategory()
        {
            return View();
        }


        [HttpPost]
        public ActionResult CreateCategory(Category category)
        {
            var newcat = new Category
            {
                Id = Guid.NewGuid(),
                CategoryName = category.CategoryName
            };
            var catlist = new List<Category>();
            cat.Add(newcat);
            return RedirectToAction("Categories");
        }
    }
}