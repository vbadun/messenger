﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public class MessageRepository : IRepository<Message>
    {
        private readonly JsonManager jm = new JsonManager();

        public void Add(Message item)
        {
            jm.Add(item);
        }
               
        public void Delete(Guid id)
        {
            jm.Delete(id);
        }

        public void Edit(Message item, Guid id)
        {
            jm.Edit(item, id);
        }
               
        public IEnumerable<Message> GetAllItems()
        {
            return jm.GetAll();
        }

        public Message GetByID(Guid id)
        {
            return jm.GetById(id);
        }

        public void SetNegativeRating(Guid id, string adress)
        {
            jm.SetNegativeRating(id, adress);
        }

        public void SetPositiveRating(Guid id, string adress)
        {
            jm.SetPositiveRating(id, adress);
        }
        
    }
}