﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    [DataContract]
    public class Message
    {

        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
     
        public string CategoryName { get; set; }
        [DataMember]
     
        public string Author { get; set; }
        [DataMember]
        public string Header { get; set; }
        [DataMember]
        public string Body { get; set; }

        [DataMember]
        public DateTime TimeOfCreate { get; set; }
        [DataMember]
        public DateTime LastModifyDate { get; set; }
        [DataMember]
        public List<string> RaitingChangeHostAddress { get; set; }
        [DataMember]
        public int Rating { get; set; }
        [DataMember]
        public string PublisherHostAddress { get; set; }
        [DataMember]
        public long BodySize { get; set; }

    }
}
