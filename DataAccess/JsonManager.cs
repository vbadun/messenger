﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DataAccess
{
    public class JsonManager
    {
        private List<Message> tmplist = new List<Message>();
        private List<Category> cattmplist = new List<Category>();
        private List<Message> savelist = new List<Message>();
        string messpath = HttpContext.Current.Server.MapPath("~/App_Data/message.json");
        string catpath = HttpContext.Current.Server.MapPath("~/App_Data/category.json");

        internal void Delete(Guid item)
        {

            var del = tmplist.Where(t => t.Id == item).Single();
            tmplist.Remove(del);
            var formatter = new DataContractJsonSerializer(typeof(List<Message>));

            using (FileStream fs = new FileStream(messpath, FileMode.Create))
            {
                formatter.WriteObject(fs, tmplist);
            }
        }

        internal Guid Add(Category item)
        {

            cattmplist = GetAllCategories().ToList();
            cattmplist.Add(item);

            var formatter = new DataContractJsonSerializer(typeof(List<Category>));

            using (FileStream fs = new FileStream(catpath, FileMode.Create))
            {
                formatter.WriteObject(fs, cattmplist);
            }
            return item.Id;

        }

        internal Message GetById(Guid id)
        {
            var message = GetAll().Where(x => x.Id == id).SingleOrDefault();
            return message;
        }

        internal void SetNegativeRating(Guid id, string adress)
        {
            tmplist = GetAll().ToList();
            if (!tmplist.Where(t => t.Id == id).Single().RaitingChangeHostAddress.Contains(adress))
            {
                tmplist.Where(t => t.Id == id).Single().Rating--;
                tmplist.Where(t => t.Id == id).Single().RaitingChangeHostAddress.Add(adress);
            }

            var formatter = new DataContractJsonSerializer(typeof(List<Message>));

            using (FileStream fs = new FileStream(messpath, FileMode.Create))
            {

                formatter.WriteObject(fs, tmplist);
            }
        }

        internal void SetPositiveRating(Guid id, string adress)
        {
            tmplist = GetAll().ToList();
            if (!tmplist.Where(t => t.Id == id).Single().RaitingChangeHostAddress.Contains(adress))
            {
                tmplist.Where(t => t.Id == id).Single().Rating++;
                tmplist.Where(t => t.Id == id).Single().RaitingChangeHostAddress.Add(adress);
            }
            var formatter = new DataContractJsonSerializer(typeof(List<Message>));

            using (FileStream fs = new FileStream(messpath, FileMode.Create))
            {

                formatter.WriteObject(fs, tmplist);
            }
        }


        internal void Edit(Message item, Guid id)
        {
            tmplist = GetAll().ToList();
            tmplist.Where(t => t.Id == id).Single().Body = item.Body;
            tmplist.Where(t => t.Id == id).Single().Header = item.Header;
            tmplist.Where(t => t.Id == id).Single().LastModifyDate = DateTime.Now;
            tmplist.Where(t => t.Id == id).Single().BodySize = item.Body.Length;
            var formatter = new DataContractJsonSerializer(typeof(List<Message>));

            using (FileStream fs = new FileStream(messpath, FileMode.Create))
            {
                formatter.WriteObject(fs, tmplist);
            }

        }

        internal Guid Add(Message item)
        {
            tmplist = GetAll().ToList();
            tmplist.Add(item);

            var formatter = new DataContractJsonSerializer(typeof(List<Message>));

            using (FileStream fs = new FileStream(messpath, FileMode.Create))
            {
                formatter.WriteObject(fs, tmplist);
            }
            return item.Id;
        }

        internal IEnumerable<Message> GetAll()
        {
            var formatter = new DataContractJsonSerializer(typeof(IEnumerable<Message>));
            using (FileStream fs = new FileStream(messpath, FileMode.OpenOrCreate))
            {
                try
                {
                    var readlist = (IEnumerable<Message>)formatter.ReadObject(fs);
                    foreach (var mes in readlist)
                    {
                        tmplist.Add(mes);
                    }
                }
                catch (Exception)
                {
                    fs.Close();
                }
            }
            return tmplist;
        }
        internal IEnumerable<Category> GetAllCategories()
        {
            var formatter = new DataContractJsonSerializer(typeof(IEnumerable<Category>));
            using (FileStream fs = new FileStream(catpath, FileMode.OpenOrCreate))
            {
                try
                {
                    var readlist = (IEnumerable<Category>)formatter.ReadObject(fs);
                    foreach (var mes in readlist)
                    {
                        cattmplist.Add(mes);
                    }
                }
                catch (Exception)
                {
                    fs.Close();
                }
            }
            return cattmplist;
        }
    }

}
