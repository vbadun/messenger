﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public interface IRepository<TModel> where TModel : class
    {
        void Add(TModel item);
        void Delete(Guid item);
        IEnumerable<TModel> GetAllItems();
        void Edit(TModel item, Guid id);
       TModel GetByID(Guid id);
    }
}
