﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class CategoryRepository : IRepository<Category>
    {
        private readonly JsonManager jm = new JsonManager();
        public void Add(Category item)
        {
           jm.Add(item);
        }

        public void Delete(Guid item)
        {
           jm.Delete(item);
        }

        public void Edit(Category item, Guid id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Category> GetAllItems()
        {
            return jm.GetAllCategories();
        }

        public Category GetByID(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
