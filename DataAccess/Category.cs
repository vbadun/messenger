﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class Category
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public string CategoryName { get; set; }
    }
}
